package com.rave.cocktailsappfragments

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Cocktail application.
 *
 * @constructor Create empty Cocktail application
 */
@HiltAndroidApp
class CocktailApplication : Application()
