package com.rave.cocktailsappfragments.model

import com.rave.cocktailsappfragments.model.local.entity.categorieslist.CategoryName
import com.rave.cocktailsappfragments.model.local.entity.drinkdetails.DrinkDetails
import com.rave.cocktailsappfragments.model.local.entity.drinkslist.Drink
import com.rave.cocktailsappfragments.model.mapper.CategoriesMapper
import com.rave.cocktailsappfragments.model.mapper.DrinksDetailsMapper
import com.rave.cocktailsappfragments.model.mapper.DrinksMapper
import com.rave.cocktailsappfragments.model.remote.ApiService
import com.rave.cocktailsappfragments.model.remote.dto.categorieslist.CategoryResponseDTO
import com.rave.cocktailsappfragments.model.remote.dto.drinkdetails.DrinkDetailsResponseDTO
import com.rave.cocktailsappfragments.model.remote.dto.drinkslist.DrinksListResponseDTO
import javax.inject.Inject

/**
 * Cocktail repo.
 *
 * @property service
 * @constructor Create empty Cocktail repo
 */
class CocktailRepo @Inject constructor(
    private val service: ApiService
) {
    val categoryMapper: CategoriesMapper = CategoriesMapper()
    val drinksMapper: DrinksMapper = DrinksMapper()
    val drinkDetailsMapper: DrinksDetailsMapper = DrinksDetailsMapper()

    /**
     * Get categories.
     *
     * @return
     */
    suspend fun getCategories(): List<CategoryName> {
        val categoryResponseDto: CategoryResponseDTO = service.getCocktailCategories()
        return categoryResponseDto.drinks.map {
            categoryMapper(it)
        }
    }

    /**
     * Get drinks in category.
     *
     * @param catName
     * @return
     */
    suspend fun getDrinksInCategory(catName: String): List<Drink> {
        val drinksResponseDto: DrinksListResponseDTO = service.getDrinksInCategory(catName)
        return drinksResponseDto.drinks.map {
            drinksMapper(it)
        }
    }

    /**
     * Get drink details.
     *
     * @param drinkName
     * @return
     */
    suspend fun getDrinkDetails(drinkName: String): List<DrinkDetails> {
        val drinkDetailsDto: DrinkDetailsResponseDTO = service.getDrinkDetails(drinkName)
        val returnedFromRepo = drinkDetailsDto.drinks.map {
            drinkDetailsMapper(it)
        }
        return returnedFromRepo
    }
}
