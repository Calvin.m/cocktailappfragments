package com.rave.cocktailsappfragments.model.local.entity.categorieslist

import kotlinx.serialization.Serializable

/**
 * Category name.
 *
 * @property strCategory
 * @constructor Create empty Category name
 */
@Serializable
data class CategoryName(
    val strCategory: String
)
