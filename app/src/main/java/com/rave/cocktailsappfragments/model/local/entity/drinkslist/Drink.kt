package com.rave.cocktailsappfragments.model.local.entity.drinkslist

import kotlinx.serialization.Serializable

@Serializable
data class Drink(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
