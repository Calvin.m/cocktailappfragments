package com.rave.cocktailsappfragments.model.mapper

import com.rave.cocktailsappfragments.model.local.entity.categorieslist.CategoryName
import com.rave.cocktailsappfragments.model.remote.dto.categorieslist.CategoryNameDTO

/**
 * Categories mapper.
 *
 * @constructor Create empty Categories mapper
 */
class CategoriesMapper : Mapper<CategoryNameDTO, CategoryName> {
    override fun invoke(dto: CategoryNameDTO): CategoryName = with(dto) {
        CategoryName(
            strCategory
        )
    }
}
