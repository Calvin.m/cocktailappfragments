package com.rave.cocktailsappfragments.model.mapper

import com.rave.cocktailsappfragments.model.local.entity.drinkdetails.DrinkDetails
import com.rave.cocktailsappfragments.model.remote.dto.drinkdetails.DrinkDetailsDTO

/**
 * Drinks details mapper.
 *
 * @constructor Create empty Drinks details mapper
 */
class DrinksDetailsMapper : Mapper<DrinkDetailsDTO, DrinkDetails> {
    override fun invoke(dto: DrinkDetailsDTO): DrinkDetails {
        return with(dto) {
            DrinkDetails(
                dateModified ?: "",
                idDrink ?: "",
                strAlcoholic ?: "",
                strCategory ?: "",
                strCreativeCommonsConfirmed ?: "",
                strDrink ?: "",
                strDrinkAlternate ?: "",
                strDrinkThumb ?: "",
                strGlass ?: "",
                strIBA ?: "",
                strImageAttribution ?: "",
                strImageSource ?: "",
                strIngredient1 ?: "",
                strIngredient10 ?: "",
                strIngredient11 ?: "",
                strIngredient12 ?: "",
                strIngredient13 ?: "",
                strIngredient14 ?: "",
                strIngredient15 ?: "",
                strIngredient2 ?: "",
                strIngredient3 ?: "",
                strIngredient4 ?: "",
                strIngredient5 ?: "",
                strIngredient6 ?: "",
                strIngredient7 ?: "",
                strIngredient8 ?: "",
                strIngredient9 ?: "",
                strInstructions ?: "",
                strInstructionsDE ?: "",
                strInstructionsES ?: "",
                strInstructionsFR ?: "",
                strInstructionsIT ?: "",
                strInstructionsZHHANS ?: "",
                strInstructionsZHHANT ?: "",
                strMeasure1 ?: "",
                strMeasure10 ?: "",
                strMeasure11 ?: "",
                strMeasure12 ?: "",
                strMeasure13 ?: "",
                strMeasure14 ?: "",
                strMeasure15 ?: "",
                strMeasure2 ?: "",
                strMeasure3 ?: "",
                strMeasure4 ?: "",
                strMeasure5 ?: "",
                strMeasure6 ?: "",
                strMeasure7 ?: "",
                strMeasure8 ?: "",
                strMeasure9 ?: "",
                strTags ?: "",
                strVideo ?: ""
            )
        }
    }
}
