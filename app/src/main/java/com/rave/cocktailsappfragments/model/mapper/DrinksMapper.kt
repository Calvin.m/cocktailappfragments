package com.rave.cocktailsappfragments.model.mapper

import com.rave.cocktailsappfragments.model.local.entity.drinkslist.Drink
import com.rave.cocktailsappfragments.model.remote.dto.drinkslist.DrinkDTO

/**
 * Drinks mapper.
 *
 * @constructor Create empty Drinks mapper
 */
class DrinksMapper : Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink {
        return with(dto) {
            Drink(
                idDrink,
                strDrink,
                strDrinkThumb
            )
        }
    }
}
