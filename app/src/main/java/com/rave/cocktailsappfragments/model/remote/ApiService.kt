package com.rave.cocktailsappfragments.model.remote

import com.rave.cocktailsappfragments.model.remote.dto.categorieslist.CategoryResponseDTO
import com.rave.cocktailsappfragments.model.remote.dto.drinkdetails.DrinkDetailsResponseDTO
import com.rave.cocktailsappfragments.model.remote.dto.drinkslist.DrinksListResponseDTO
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api service.
 *
 * @constructor Create empty Api service
 */
interface ApiService {
    @GET(COCKTAIL_CATEGORIES_ENDPOINT)
    suspend fun getCocktailCategories(@Query("c") categoryList: String = "list"): CategoryResponseDTO

    @GET(COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT)
    suspend fun getDrinksInCategory(@Query("c") categoryName: String): DrinksListResponseDTO

    @GET(COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT)
    suspend fun getDrinkDetails(@Query("s") drinkName: String): DrinkDetailsResponseDTO

    companion object {
        private const val COCKTAIL_CATEGORIES_ENDPOINT = "list.php"
        private const val COCKTAIL_DRINKS_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val COCKTAIL_SELECTED_DRINK_DETAILS_ENDPOINT = "search.php"
    }
}
