package com.rave.cocktailsappfragments.model.remote.dto.categorieslist

import kotlinx.serialization.Serializable

@Serializable
data class CategoryNameDTO(
    val strCategory: String
)
