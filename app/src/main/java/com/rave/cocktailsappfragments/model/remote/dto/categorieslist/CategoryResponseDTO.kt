package com.rave.cocktailsappfragments.model.remote.dto.categorieslist

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponseDTO(
    val drinks: List<CategoryNameDTO>
)
