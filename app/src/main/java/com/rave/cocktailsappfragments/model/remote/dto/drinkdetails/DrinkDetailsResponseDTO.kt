package com.rave.cocktailsappfragments.model.remote.dto.drinkdetails

import kotlinx.serialization.Serializable

@Serializable
data class DrinkDetailsResponseDTO(
    val drinks: List<DrinkDetailsDTO> = emptyList()
)
