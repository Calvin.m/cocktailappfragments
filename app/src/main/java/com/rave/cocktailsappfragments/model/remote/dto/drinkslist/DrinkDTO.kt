package com.rave.cocktailsappfragments.model.remote.dto.drinkslist

import kotlinx.serialization.Serializable

@Serializable
data class DrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
