package com.rave.cocktailsappfragments.model.remote.dto.drinkslist

import kotlinx.serialization.Serializable

@Serializable
data class DrinksListResponseDTO(
    val drinks: List<DrinkDTO>
)
