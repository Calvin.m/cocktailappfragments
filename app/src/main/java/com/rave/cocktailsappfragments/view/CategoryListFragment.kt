package com.rave.cocktailsappfragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.cocktailsappfragments.R
import com.rave.cocktailsappfragments.viewmodel.CocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Category list fragment.
 *
 * @constructor Create empty Category list fragment
 */
@AndroidEntryPoint
class CategoryListFragment : Fragment() {
    private val cocktailViewModel by activityViewModels<CocktailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        cocktailViewModel.fetchCategories()
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = cocktailViewModel.state.collectAsState().value
                LazyColumn() {
                    items(state.categoryList, key = { it.strCategory }) { item ->
                        Text(
                            text = item.strCategory,
                            modifier = Modifier.clickable {
                                cocktailViewModel.selectCategory(item.strCategory)
                                findNavController().navigate(R.id.drinkListFragment)
                            }
                        )
                    }
                }
            }
        }
    }
}
