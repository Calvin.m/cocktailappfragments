package com.rave.cocktailsappfragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import coil.compose.AsyncImage
import com.rave.cocktailsappfragments.viewmodel.CocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Drink details fragment.
 *
 * @constructor Create empty Drink details fragment
 */
@AndroidEntryPoint
class DrinkDetailsFragment : Fragment() {
    private val cocktailViewModel by activityViewModels<CocktailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = cocktailViewModel.state.collectAsState().value
                if (state.isLoading) {
                    CircularProgressIndicator()
                }
                if (state.drinkDeets.size > 0) {
                    LazyColumn() {
                        items(state.drinkDeets) { drink ->
                            AsyncImage(model = drink.strDrinkThumb, contentDescription = null)
                            Text(drink.strDrink)
                        }
                    }
                }
            }
        }
    }
}
