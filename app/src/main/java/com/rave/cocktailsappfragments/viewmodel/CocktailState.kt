package com.rave.cocktailsappfragments.viewmodel

import com.rave.cocktailsappfragments.model.local.entity.categorieslist.CategoryName
import com.rave.cocktailsappfragments.model.local.entity.drinkdetails.DrinkDetails
import com.rave.cocktailsappfragments.model.local.entity.drinkslist.Drink

/**
 * Cocktail state.
 *
 * @property isLoading
 * @property categoryList
 * @property selectedCategory
 * @property drinksList
 * @property drinkDeets
 * @constructor Create empty Cocktail state
 */
data class CocktailState(
    val isLoading: Boolean = false,
    val categoryList: List<CategoryName> = emptyList(),
    val selectedCategory: String = "",
    val drinksList: List<Drink> = emptyList(),
    val drinkDeets: List<DrinkDetails> = emptyList()
)
