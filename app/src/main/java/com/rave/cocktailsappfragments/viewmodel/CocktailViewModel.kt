package com.rave.cocktailsappfragments.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailsappfragments.model.CocktailRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Cocktail view model.
 *
 * @property repo
 * @constructor Create empty Cocktail view model
 */
@HiltViewModel
class CocktailViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {

    private val _state = MutableStateFlow(CocktailState())
    val state get() = _state.asStateFlow()

    /**
     * Fetch categories.
     *
     */
    fun fetchCategories() {
        _state.update { it.copy(isLoading = true) }
        viewModelScope.launch {
            val res = repo.getCategories()
            _state.update { it.copy(isLoading = false, categoryList = res) }
        }
    }

    /**
     * Select category.
     *
     * @param catName
     */
    fun selectCategory(catName: String) = viewModelScope.launch {
        _state.update { it.copy(isLoading = true) }
        val drinksInCategory = repo.getDrinksInCategory(catName)
        _state.update { it.copy(isLoading = false, drinksList = drinksInCategory) }
    }

    /**
     * Select drink.
     *
     * @param drinkName
     */
    fun selectDrink(drinkName: String) = viewModelScope.launch {
        _state.update { it.copy(isLoading = true) }
        val drinkDetails = repo.getDrinkDetails(drinkName)
        _state.update { it.copy(isLoading = false, drinkDeets = drinkDetails) }
    }
}
